/*global chrome*/
import React, { Component } from "react";
import axios from "axios";
import Autocomplete from "react-autocomplete";
import logo from "../assets/img/ApopougrLogo-2.png";
// import $ from 'jquery';
import "bootstrap/dist/js/bootstrap";

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Gaurav",
      apiUrl: "https://api.apopou.gr/v1/retailers",
      featureApi: "http://api.apopou.gr/v1/retailers?sort_by=featured",
      dealWeekApi: "http://api.apopou.gr/v1/retailers?sort_by=deal_of_week",
      mostVisitedApi: "http://api.apopou.gr/v1/retailers?sort_by=visits",
      active: 1,
      retailers: [],
      liHtml: [],
      loading: true,
      config: {
        headers: {
          "content-type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
      },
      logo: logo,
      loadMore: "https://apopou.gr/online-katasthmata",
      back: false,
      appoLink: "",
      finalLoadMore: "",
      value: "",
      autocompleteApi: "http://api.apopou.gr/v1/autocomplete",
      autocomplete: [],
      loggedIn: 0,
      logoUrl: "https://apopou.gr/",
      height: 0,
      error: false,
      logoContent: "",
      noData: 0
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClickChild = this.handleClickChild.bind(this);
    this.newTab = this.newTab.bind(this);
    this.retailer = this.retailer.bind(this);
    this.coupon = this.coupon.bind(this);
    this.searchAutocomplete = this.searchAutocomplete.bind(this);
  }

  handleClickChild(id, e) {
    console.log(e);
    console.log("id", id);
    this.setState({
      loading: true,
      back: true
    });

    try {
      this.coupon(id);
    } catch (e) {
      this.coupon(id);
    }
  }

  handleClick(active) {
    if (active === 4) {
      this.newTab("https://apopou.gr/help");
    } else if (active === 5) {
      this.newTab("https://www.facebook.com/ApoPouGR/");
    } else if (active === 7) {
      this.newTab("https://twitter.com/apo_pou");
    } else if (active === 6) {
      if (this.state.loggedIn === 1) {
        this.newTab("https://apopou.gr/logout.php");
      } else {
        this.newTab("https://apopou.gr/login");
      }
    } else {
      this.setState(
        {
          active: active
        },
        function() {
          this.retailer();
        }
      );
    }
  }

  newTab(url) {
    window.close();
    chrome.tabs.create({ url: url });
  }

  searchAutocomplete(search) {
    this.setState({
      value: search
    });
    console.log(search);
    const This = this;
    const autocomplete = axios
      .get(this.state.autocompleteApi + "?query=" + search, this.state.config)
      .then(function(response) {
        This.setState({
          autocomplete: response.data.data
        });
      })
      .catch(function(e) {
        autocomplete();
      });
    // This.setState({
    //   autocomplete: [],
    // })
  }

  coupon(id) {
    const This = this;
    axios
      .get(this.state.apiUrl + "/" + id, this.state.config)
      .then(function(response) {
        This.setState({
          retailers: response.data,
          logo: response.data.image,
          logoUrl: response.data.apopoulink,
          logoContent: (
            <div
              className="logo-content"
              style={{ textAlign: "center", color: "#20c048" }}
              onClick={() => {
                This.newTab(response.data.apopoulink);
              }}
            >
              <span
                className="big-text"
                style={{
                  display: "block",
                  margin: "0px 25px 1px 1px",
                  fontSize: "19px"
                }}
              >
                Πήγαινε στο κατάστημα
              </span>
              <span
                className="small-text"
                style={{
                  display: "block",
                  margin: "0px 25px 1px 1px",
                  fontSize: "10px"
                }}
              >
                {response.data.cashback} Cashback/ Επιστροφή Χρημάτων,{" "}
                {response.data.couponstotal} Διαθέσιμα Κουπόνια
              </span>
            </div>
          )
        });
        var liHtml = [];
        console.log(
          "response.data.coupons.length",
          response.data.coupons.length
        );
        if (response.data.coupons.length > 0) {
          const data = response.data.coupons;
          const mainData = response.data;
          var i = 0;
          while (i < data.length) {
            let link = data[i].apopoulink;
            liHtml.push(
              <div
                className="tab-pane-child horizontal-child"
                style={{ marginTop: "7px" }}
                onClick={() => {
                  This.newTab(link);
                }}
              >
                <div className="content-retailer">
                  <div className="content-retailer-child">
                    <div style={{ width: "100%" }}>
                      <img src={mainData.image} alt="" />
                      <span
                        style={{
                          fontWeight: "bold",
                          lineHeight: "1",
                          color: "white"
                        }}
                      >
                        {data[i].coupon_type === "coupon" && (
                          <span
                            style={{
                              backgroundColor: "#ff2552",
                              padding: "2px"
                            }}
                          >
                            ΚΩΔΙΚΟΣ ΚΟΥΠΟΝΙΟΥ
                          </span>
                        )}
                        {data[i].coupon_type === "flyer" && (
                          <span
                            style={{ backgroundColor: "brown", padding: "2px" }}
                          >
                            ΦΥΛΛΑΔΙΟ ΠΡΟΣΦΟΡΩΝ
                          </span>
                        )}
                        {data[i].coupon_type === "discount" && (
                          <span
                            style={{
                              backgroundColor: "#0394ea",
                              padding: "2px"
                            }}
                          >
                            ΠΡΟΣΦΟΡΑ
                          </span>
                        )}
                        {data[i].coupon_type === "cashback" && (
                          <span
                            style={{ backgroundColor: "grey", padding: "2px" }}
                          >
                            ΕΠΙΣΤΡΟΦΕΣ ΜΕΤΡΗΤΩΝ
                          </span>
                        )}
                        {data[i].coupon_type === "printable" && (
                          <span
                            style={{
                              backgroundColor: "#8f13fd",
                              padding: "2px"
                            }}
                          >
                            ΕΚΤΥΠΩΣΩΣΙΜΟ
                          </span>
                        )}
                      </span>
                      <div style={{ fontSize: "15px", marginBottom: "2px" }}>
                        {data[i].title != "" ? data[i].title : ""}
                      </div>
                      {mainData.cashback != "" && (
                        <div
                          style={{
                            color: "#f30",
                            fontWeight: "bold",
                            marginBottom: "2px"
                          }}
                        >
                          + Επιπλέον {mainData.cashback} ΕΠΙΣΤΡΟΦΕΣ ΜΕΤΡΗΤΩΝ
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            );
            i++;
          }
          This.setState({
            noData: 0
          });
        } else {
          liHtml.push(
            <div
              className="tab-pane-child horizontal-child"
              style={{ marginTop: "9px" }}
            >
              <div className="content-retailer">
                <div className="content-retailer-child">
                  <div style={{ width: "100%", fontSize: "20px" }}>
                    Δεν υπάρχουν αυτή τη στιγμή διαθέσιμα κουπόνια για αυτό το
                    κατάστημα
                  </div>
                </div>
              </div>
            </div>
          );
          This.setState({
            noData: 1
          });
          console.log("else");
        }
        This.setState({
          liHtml: liHtml,
          loading: false,
          finalLoadMore: response.data.apopoulink
        });
      })
      .catch(function(e) {
        console.log(e);
        This.setState({
          loading: false,
          error: true
        });
      });
  }

  retailer() {
    this.setState({
      loading: true,
      back: false,
      finalLoadMore: this.state.loadMore,
      logo: logo,
      logoUrl: "https://apopou.gr/",
      noData: 0
    });
    const This = this;
    let api = this.state.featureApi;
    if (this.state.active === 2) {
      api = this.state.mostVisitedApi;
    } else if (this.state.active === 3) {
      api = this.state.dealWeekApi;
    }
    axios
      .get(api, this.state.config)
      .then(function(response) {
        This.setState({
          retailers: response.data
        });
        var liHtml = [];
        if (response.data.data.length > 0) {
          const data = response.data.data;
          var i = 0;
          while (i < data.length) {
            let id = data[i].retailer_id;
            liHtml.push(
              <div
                className="tab-pane-child"
                onClick={() => {
                  This.handleClickChild(id);
                }}
                data-id={id}
              >
                <div style={{ marginBottom: "2px" }}>
                  <img
                    style={{
                      width: "75px",
                      marginBottom: "5px",
                      alignSelf: "center"
                    }}
                    src={data[i].image}
                    alt="logo"
                  />
                </div>
                <div className="content-retailer">
                  <div className="content-retailer-child">
                    <div style={{ width: "100%", textAlign: "center" }}>
                      <div>
                        <span
                          style={{
                            fontSize: "16px",
                            fontWeight: "700",
                            color: "#f30"
                          }}
                        >
                          {data[i].cashback} Cashback
                        </span>
                      </div>
                      <div style={{ fontSize: "12px", color: "#888" }}>
                        + Κουπόνια {data[i].title != "" ? data[i].title : ""}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
            i++;
          }
          This.setState({
            liHtml: liHtml,
            loading: false
          });
        }
      })
      .catch(function(e) {
        This.setState({
          loading: false,
          error: true
        });
      });
  }

  componentDidMount() {
    const $this = this;
    this.retailer();
    chrome.cookies.get(
      {
        url: "https://apopou.gr/",
        name: "apopougr_logged_in"
      },
      function(res) {
        console.log("cookie", res);
        if (res) {
          if (res.value == "true") {
            console.log("haoo", res);
            $this.setState({
              loggedIn: 1
            });
          }
        }
      }
    );
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { message: "height" }, function(
        response
      ) {
        if (chrome.runtime.lastError) {
          // console.warn('Error: ' + chrome.runtime.lastError.message);
        } else {
          $this.setState({
            height: response - (response * 7) / 100
          });
        }
      });
    });
  }
  render() {
    if (this.state.height == 0) {
      this.setState({
        height: 550
      });
    }
    if (this.state.loading === true) {
      return (
        <div className="login-form" style={{ maxHeight: this.state.height }}>
          <div className="loading"></div>
        </div>
      );
    } else if (this.state.error === true) {
      return (
        <div
          className="login-form"
          style={{
            textAlign: "center",
            padding: 50,
            backgroundColor: "#0000007a",
            color: "#fff",
            maxHeight: this.state.height
          }}
        >
          <span style={{ display: "table-header-group" }}>
            Something went wrong, Please try again.
          </span>
        </div>
      );
    } else {
      return (
        <div className="login-form" style={{ maxHeight: this.state.height }}>
          <nav
            className="navbar navbar-expand-lg"
            style={{ backgroundColor: ["#20c048", true] }}
          >
            <div className="container">
              {this.state.back === true && (
                <span className="site-icon back" onClick={this.retailer}>
                  <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </span>
              )}
              <div
                className="site-logo"
                style={
                  this.state.back === true
                    ? { textAlign: "left", marginLeft: "43px" }
                    : { textAlign: "center" }
                }
              >
                <a
                  href="#test"
                  style={
                    this.state.back === true
                      ? { display: "flex", textDecoration: "none" }
                      : { display: "block" }
                  }
                >
                  <img
                    src={this.state.logo}
                    onClick={() => {
                      this.newTab(this.state.logoUrl);
                    }}
                    alt="Logo"
                    style={{ maxHeight: "55px" }}
                  />
                  {this.state.back === true && this.state.logoContent}
                </a>
              </div>
              <span
                className="site-icon close"
                onClick={() => {
                  window.close();
                }}
              >
                <i class="fa fa-times" aria-hidden="true"></i>
              </span>
            </div>
            <div className="text-center search-button">
              <span className="search-icon">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
              <Autocomplete
                items={this.state.autocomplete}
                shouldItemRender={(item, value) =>
                  item.title.toLowerCase().indexOf(value.toLowerCase()) > -1
                }
                getItemValue={item => item.title}
                renderItem={(item, highlighted) => (
                  <div
                    onClick={() => {
                      this.newTab(item.apopoulink);
                    }}
                    key={item.retailer_id}
                    style={{ backgroundColor: highlighted ? "#eee" : "#fff" }}
                  >
                    <span
                      style={{
                        cursor: "pointer",
                        display: "block",
                        padding: "2px 12px",
                        borderBottom: "1px solid rgba(34,36,38,.1)"
                      }}
                      onClick={() => {
                        this.newTab(item.apopoulink);
                      }}
                    >
                      <span
                        className="auto-title"
                        style={{
                          width: "70%",
                          textAlign: "left",
                          display: "inline-block"
                        }}
                      >
                        {item.title}
                      </span>
                      <span
                        className="auto-coupon"
                        style={{
                          width: "30%",
                          textAlign: "right",
                          display: "inline-block"
                        }}
                      >
                        {item.cashback !== "Ν/Α"
                          ? item.cashback
                          : `${item.couponstotal} Κουπόνια`}
                      </span>
                    </span>
                  </div>
                )}
                value={this.state.value}
                onChange={e => this.searchAutocomplete(e.target.value)}
                inputProps={{ placeholder: "Search for stores" }}
              />
            </div>
            <div className="text-center head-tabs">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a
                    className={
                      this.state.active === 1 ? "nav-link active" : "nav-link"
                    }
                    onClick={() => {
                      this.handleClick(1);
                    }}
                    href="#test"
                  >
                    Προτεινόμενα Καταστήματα
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={
                      this.state.active === 2
                        ? "nav-link active contact-us"
                        : "nav-link contact-us"
                    }
                    onClick={() => {
                      this.handleClick(2);
                    }}
                    href="#test"
                  >
                    Δημοφιλή Καταστήματα
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={
                      this.state.active === 3 ? "nav-link active" : "nav-link"
                    }
                    onClick={() => {
                      this.handleClick(3);
                    }}
                    href="#test"
                  >
                    Καταστήματα Εβδομάδας
                  </a>
                </li>
                {/* <li className="nav-item">
                    <a className={this.state.active === 2 ? "nav-link active": "nav-link"} href="#test" onClick={ () =>{this.handleClick(2)}}>Profile</a>
                  </li> */}
              </ul>
            </div>
          </nav>
          <div className="main-content">
            <div className="tab-content">
              <div
                className={
                  this.state.active === 1
                    ? "tab-pane fade show active"
                    : "tab-pane fade"
                }
              >
                {this.state.liHtml}
              </div>
              <div
                className={
                  this.state.active === 2
                    ? "tab-pane fade show active"
                    : "tab-pane fade"
                }
              >
                {this.state.liHtml}
              </div>
              <div
                className={
                  this.state.active === 3
                    ? "tab-pane fade show active"
                    : "tab-pane fade"
                }
              >
                {this.state.liHtml}
              </div>
            </div>
          </div>
          <div className="load-more">
            <a
              href="#test"
              onClick={() => {
                this.newTab(this.state.finalLoadMore);
              }}
            >
              Δες περισσότερα
            </a>
          </div>
          <div className="fixed-footer">
            <ul className="nav nav-tabs">
              <li className="nav-item">
                <a
                  className={
                    this.state.active === 6 ? "nav-link active" : "nav-link"
                  }
                  onClick={() => {
                    this.handleClick(6);
                  }}
                  href="#test"
                >
                  {this.state.loggedIn == 1 ? (
                    <span>
                      <i class="fa fa-sign-out" aria-hidden="true"></i> Ο
                      Λογαριασμός μου
                    </span>
                  ) : (
                    <span>
                      <i class="fa fa-sign-in" aria-hidden="true"></i>{" "}
                      Σύνδεση/Εγγραφή
                    </span>
                  )}
                </a>
              </li>
              <li className="nav-item">
                <a
                  className={
                    this.state.active === 4 ? "nav-link active" : "nav-link"
                  }
                  onClick={() => {
                    this.handleClick(4);
                  }}
                  href="#test"
                >
                  <i class="fa fa-question" aria-hidden="true"></i>{" "}
                  Βοήθεια/Επικοινωνία
                </a>
              </li>
              <li className="nav-item fb">
                <a
                  className={
                    this.state.active === 5 ? "nav-link active fb" : "nav-link"
                  }
                  onClick={() => {
                    this.handleClick(5);
                  }}
                  href="#test"
                >
                  <i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;
                </a>
              </li>
              <li className="nav-item fb">
                <a
                  className={
                    this.state.active === 7 ? "nav-link active fb" : "nav-link"
                  }
                  onClick={() => {
                    this.handleClick(7);
                  }}
                  href="#test"
                >
                  <i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;
                </a>
              </li>
              {/* <li className="nav-item">
                  <a className={this.state.active === 2 ? "nav-link active": "nav-link"} href="#test" onClick={ () =>{this.handleClick(2)}}>Profile</a>
                </li> */}
            </ul>
          </div>
        </div>
      );
    }
  }
}
