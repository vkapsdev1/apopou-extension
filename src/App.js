import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Dashboard from './components/dashboard.component';
import Index from './components/index.component';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Switch>
              <Route path='/dashboard' component={ Dashboard } />
              <Route path='/index' component={ Dashboard } />
              <Route path='/' component={ Dashboard } />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
