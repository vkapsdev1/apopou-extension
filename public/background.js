chrome.tabs.onUpdated.addListener(function(e, s, a) {
  function singleUrl() {
    chrome.tabs.sendMessage(
      e,
      {
        message: "Page Refresh",
        url: s.url
      },
      function(url) {
        if (chrome.runtime.lastError) {
          // console.warn('Error: ' + chrome.runtime.lastError.message);
          singleUrl(chrome);
        } else {
          if (url !== undefined && url !== "" && url !== null) {
            console.log("refresh", url);

            fetch("https://api.apopou.gr/v1/search?domain=" + url)
              .then(function(response) {
                return response.json();
              })
              .then(function(json) {
                chrome.tabs.sendMessage(e, {
                  message: "refresh",
                  action: json
                });
              })
              .catch(error => {});
          }
        }
      }
    );
  }
  chrome.tabs.sendMessage(
    e,
    {
      message: "Hot reload",
      url: s.url
    },
    async function(url) {
      if (chrome.runtime.lastError) {
        // console.warn('Error: ' + chrome.runtime.lastError.message);
      } else {
        const urLength = url.length;
        var i = 0;
        while (i < urLength) {
          var j = 0;
          var k = 0;
          while (j < i) {
            if (url[i].url == url[j].url) {
              k = 1;
              if (url[j].data != null) {
                url[i].find = true;
                url[i].data = url[j].data;
              }
            }
            j++;
          }
          if (url[i].url !== undefined && url[i].url !== "" && k == 0) {
            await fetch("https://api.apopou.gr/v1/search?domain=" + url[i].url)
              .then(function(response) {
                return response.json();
              })
              .then(function(json) {
                if (!json.error) {
                  url[i].find = true;
                  url[i].data = json;
                }
              });
          }

          i++;
        }

        chrome.tabs.sendMessage(e, {
          message: "Data",
          action: url
        });
        // if(url !== undefined && url !== ""){
        // 	fetch("https://api.apopou.gr/v1/search?domain="+url).then(function (response) {
        // 			return response.json();
        // 	}).then(function (json) {
        // 		chrome.tabs.sendMessage(e,{
        // 			message:"Data",
        // 			action: json
        // 		})
        // 	});
        // }
      }
    }
  );
  if (s.url) {
    singleUrl();
  }
  return true;
});

chrome.runtime.onInstalled.addListener(function(details) {
  if (details.reason == "install") {
    console.log("This is a first install!");
  }

  chrome.storage.local.remove(["apopou_urls"]);
  return true;
});
